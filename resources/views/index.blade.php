@extends('template')
@section('title', 'Dashboard')

@section('textHeader') ¡Bienvenido, Usuario! @endsection

@section('header')
<div class="row">
    <div class="col-md">
        <div class="card border-bottom-primary shadow mb-4">
            <div class="card-body">
                <div class="row no-glutters align-items-center">
                    <div class="col-9">
                        <h2 class="text-primary">Usuarios registrados:</h2>
                        <p class="text-default mb-0">1,342 usuarios</p>
                    </div>
                    <div class="col-auto">
                    <i class="fa fa-user fa-3x text-primary ml-3"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md">
        <div class="card border-bottom-danger shadow mb-4">
            <div class="card-body">
                <h2 class="text-danger">Anuncios:</h2>
                <p class="text-default mb-0">No tiene ningún anuncio...</p>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row mb-4">
    <div class="col">
        <div class="card shadow">
            <div class="card-header bg-primary h2 text-white mb-0">Gastos del mes</div>
            <div class="card-body">
            <div class="chart-bar">
                <div class="chartjs-size-monitor">
                    <div class="chartjs-size-monitor-expand">
                        <div class=""></div>
                    </div>
                    <div class="chartjs-size-monitor-shrink">
                        <div class=""></div>
                    </div>
                </div>
                    <canvas id="myBarChart" width="1244" height="640" class="chartjs-render-monitor" style="display: block; height: 320px; width: 622px;">
                    </canvas>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="card shadow">
            <div class="card-header bg-info">
                <h2 class="text-white border-0 mb-0">Clientes</h2>
            </div>
                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                    <thead class="thead-light">
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone number</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Isaac</td>
                            <td>example@email.com</td>
                            <td>987463723</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Michael</td>
                            <td>example2@emailcom</td>
                            <td>932839483</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Maria</td>
                            <td>Maria23@example.com</td>
                            <td>129847323</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Itzel</td>
                            <td>Itzel954@example.com</td>
                            <td>985748302</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Jun Ho</td>
                            <td>JunhoNuneo@example.com</td>
                            <td>344333499</td>
                        </tr>
                    </tbody>
                    </table>
                </div>
        </div>
    </div>
</div>
@endsection