@extends('template')

@section('title','Generar Gastos')

@section('header')
@if (session('success'))
<div class="sufee-alert alert with-close alert-success alert-dismissible fade show" role="alert">
    {{ session()->get('success') }}	
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<a href="{{url('/gastos')}}" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left"></i> Regresar</a>

@endsection

@section('content')
<div class="row">
    <div class="col-lg-8 mx-auto">
        <div class="card shadow">
            <div class="card-header bg-info text-center">
                <h1 class="text-white mb-0">Formulario de gastos</h1>
            </div>
            <div class="card-body">
                <ul class="nav nav-pills-info mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a href="#manual-pane" class="nav-link active" id="manual-tab" data-toggle="pill" role="tab" aria-controls="manual-pane" aria-selected="true">Datos Manual</a>
                    </li>
                    <li class="nav-item">
                        <a href="#archivo-pane" class="nav-link" id="archivo-tab" data-toggle="pill" role="tab" aria-controls="archivo-pane" aria-selected="true">Subir foto o archivo</a>
                    </li>
                </ul>
                    <div class="tab-content">

                        <div class="tab-pane fade" id="archivo-pane" role="tabpanel" aria-labelledby="archivo-tab">
                        <form action="{{ url('generar_gasto/crear_2')}}" enctype="multipart/form-data" method="post">
                        {!! csrf_field() !!} 
                            <div class="row">

                                <div class="form-group col-md-6">
                                    <label class="text-info" for="concepto">Concepto *</label>
                                    <input type="text" class="form-control" 
                                           name="concepto"
                                           pattern="[A-Z a-z]+" value="{{ old('concepto') }}" >
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="text-info" for="archivo">Archivo *</label>
                                    <input type="file" class="form-control-file border"
                                           name="archivo"
                                           >
                                </div>

                                <input type="hidden" name="status" value="proceso">
                                <input type="hidden" name="usuarioid" value="{{ auth()->user()->id }}">
                                <input type="hidden" name="empresaid" value="{{ auth()->user()->empresas_id}}">
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <p class="mb-0">* Los campos marcados con asteriscos son obligatorios</p>
                                </div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn  btn-info float-right">Crear</button>
                                </div>
                            </div>
                        </form>
                        </div>
                        <div class="tab-pane fade show active" id="manual-pane" role="tabpanel" aria-labelledby="manual-tab">
                            <form action="{{url('generar_gasto/crear_1')}}" method="post">
                            
                            {{ csrf_field() }}
                            <div class="row">

                                <div class="form-group col-md-4">
                                    <label class="text-info" for="memo">Memo *</label>
                                    <input type="text" class="form-control" 
                                           name="memo" placeholder="Concepto o motivo"
                                           pattern="[A-Z a-z]+" value="{{ old('memo') }}" >
                                </div>

                                <div class="form-group col-md-4">
                                    <label class="text-info" for="tipo_pago">Tipo de pago *</label>
                                    <select name="tipopago" id="tipo_pago" class="form-control">
                                        <option value="">Seleccionar forma de pago</option>
                                        <option value="efectivo">Efectivo</option>
                                        <option value="tarjeta ">Tarjeta</option>
                                    </select>
                                </div>

                                <div class="form-group col-md-4">
                                    <label class="text-info" for="pais">Pais *</label>
                                    <input type="text" class="form-control" 
                                           name="pais" placeholder="Pais"
                                           pattern="[A-Z a-z]+" value="{{ old('pais') }}">
                                </div>


                                <div class="form-group col-md-8">
                                    <label class="text-info" for="comentario">Comentario *</label>
                                    <input type="text" class="form-control" 
                                           name="comentario" placeholder="Alimentos"
                                           pattern="[A-Z a-z]+" value="{{ old('comentario') }}" >
                                </div>

                                <div class="form-group col-md-4">
                                    <label class="text-info" for="fecha">Fecha *</label>
                                    <input  type="text" 
                                    class="form-control calen" 
                                           name="fecha" value="{{ old('fecha') }}" >
                                </div>

                                <div class="form-group col-md-4">
                                    <label class="text-info" for="orden_servicio">Tipo de cambio *</label>
                                    <input type="text" class="form-control" placeholder="1.0"
                                           name="tipocambio" value="{{ old('tipocambio') }}">
                                </div>

                                <div class="form-group col-md-4">
                                    <label class="text-info" for="moneda">Moneda *</label>
                                    <select name="moneda" id="moneda" class="form-control">
                                        <option value="">Seleccionar Moneda</option>
                                        <option value="MXN ">MXN-Pesos Mexicanos</option>
                                        <option value="USD ">USD-Dolar EstadoUnidenses</option>
                                        <option value="EUR ">EUR-Euro</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row d-flex justify-content-end mt-5">
                                <div class="form-group col-md-4 ">
                                    <label class="text-info" for="monto">Monto Total *</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">$</span>
                                        </div>
                                        <input type="text" class="form-control " 
                                               name="monto" value="{{ old('monto') }}">
                                        <div class="input-group-append">
                                            <span class="input-group-text">MXN</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" name="status" value="proceso">
                            <input type="hidden" name="usuarioid" value="{{ auth()->user()->id }}">
                            <input type="hidden" name="empresaid" value="{{ auth()->user()->empresas_id}}">
                            <div class="row">
                                <div class="col-md-8">
                                    <p class="mb-0">* Los campos marcados con asteriscos son obligatorios</p>
                                </div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn  btn-info float-right">Crear</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
@endsection