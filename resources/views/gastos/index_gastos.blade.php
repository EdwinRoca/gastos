@extends('template')

@section('title','Listado de Gastos')

@section('content')
<div clas="col-12">
    <a href="/generar_gasto" class="btn btn-primary" >Agregar Gastos</a>
    <a href="/upload" class="btn btn-primary" >Agregar Gastos por Factura Electronica</a>
</div>
<div class="row mt-4">
    <div class="col-lg mx-auto">
        <div class="card shadow border-bottom-primary">
            <div class="card-body table-responsive">
                <table id="usuarios" class="table align-items-center table-hover table-borderless table-flush">
                    <thead class="thead-light">
                        <th>#</th>
                        <th>Usuario</th>
                        <th>Empresa</th>
                        <th>Archivo</th>
                        <th>Concepto</th>
                        <th>Memo</th>
                        <th>Tipo de pago</th>
                        <th>Pais</th>
                        <th>Comentario</th>
                        <th>Fecha</th>
                        <th>Tipo de cambio</th>
                        <th>Moneda</th>
                        <th>Monto</th>
                        <th>Estatus</th>
                    </thead>
                    <tbody>
                        @foreach($gastos as $gasto)
                        <tr>
                            <td>{{$gasto->id}}</td>
                            <td>{{$gasto->usuario->nombre}}</td>
                            <td>{{$gasto->empresa->nombre}}</td>
                            @if($gasto->archivo)
                            <td>
                            <a class="btn btn-success" href="{{Storage::url($gasto->archivo)}}" target="_blank"><i class="far fa-file-alt"></i></a>
                            </td>
                            @else 
                            <td></td>
                            @endif
                            <td>{{$gasto->concepto}}</td>
                            <td>{{$gasto->memo}}</td>
                            <td>{{$gasto->tipo_pago}}</td>
                            <td>{{$gasto->pais}}</td>
                            <td>{{$gasto->comentario}}</td>
                            <td>{{$gasto->fecha}}</td>
                            <td>{{$gasto->tipo_cambio}}</td>
                            <td>{{$gasto->moneda}}</td>
                            <td>{{$gasto->monto}}</td>
                            <td>{{$gasto->estatus}}</td>
                            
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection