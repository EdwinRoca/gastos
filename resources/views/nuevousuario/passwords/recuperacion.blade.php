@extends('template2')

@section('title','Recuperar Contraseña')

@section('header','Recuperar Contraseña')

@section('content')            
<div class="col-lg-6 d-none d-lg-block bg-password-image"></div>
<div class="col-lg-6">
  <div class="p-5">
    <div class="text-center">
      <h1 class="h4 text-gray-900 mb-2">¿Olvidaste tu contraseña?</h1>
      <p>Lo entendemos, a veces pasa...</p>
      <p  class="mb-4">Ingresa tu <strong class="text-primary">Correo Electrónico</strong> y nosotros te enviaremos un enlace para que puedas reiniciar tu contraseña!</p>
    </div>
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <form class="user" action="{{ route('send_email') }}" method="POST">
    @csrf
      <div class="form-group">
        <input type="email" class="form-control form-control-user {{ $errors->has('email') ? ' is-invalid' : '' }}" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Ingresa tu Correo electrónico..." name="email" required>
      @if ($errors->has('email'))
          <span class="invalid-feedback d-block" role="alert">
              <strong>{{ $errors->first('email') }}</strong>
          </span>
      @endif
      </div>
      <div>
        <input class="btn btn-primary btn-user btn-block" type="submit" value="Reiniciar contraseña">
      </div>
    </form>
    <hr>
    <div class="text-center">
      <a class="small" href="{{url('login')}}">¿Ya tienes una cuenta? Inicia sesión!</a>
    </div>
  </div>
</div>
@endsection