@extends('template')

@section('title','Notificaciones')


@section('header')
<a href="{{url('/')}}" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left"></i> Regresar</a>
@endsection

@section('content')
    @if(!empty($unread))
        @foreach ($unread as $unre)
        <a class="text-decoration-none" href="{{ url('generar_gasto') }}">
            <div class="box-notification">
                <strong>{{ $unre->data['datos'] }}</strong>
                <div class="small text-gray-500"><strong>{{ \Carbon\Carbon::parse($unre->created_at)->diffForHumans() }}</strong></div>
            </div>
        </a>
        @endforeach
    @endif
    @if(!empty($read))
        @foreach($read as $rea)
        <a class="text-decoration-none" href="{{ url('generar_gasto') }}">
            <div class="box-notification">
                {{ $rea->data['datos'] }}
                <div class="small text-gray-500">{{ \Carbon\Carbon::parse($rea->created_at)->diffForHumans() }}</div>
            </div>
        </a>
        @endforeach
    @endif
@endsection