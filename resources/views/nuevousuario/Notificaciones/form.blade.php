<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Prueba de Notificaciones</title>
</head>
<body>
    <form action="{{ route('form') }}" method="POST">
        @csrf
        <label for="name">Escribe tu nombre</label>
        <input type="text" name="name">
        <label for="lastname">Escribe tus apellidos</label>
        <input type="text" name="lastname">
        <label for="description">Descripción</label>
        <input type="text" name="description">
        <input type="submit" value="Enviar">
    </form>
</body>
</html>