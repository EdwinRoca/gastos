@extends('errors.errorTemplate')

@section('title', 'Error 404')

@section('content')
<div class="container-fluid my-5">
    <div class="h1 text-center text-primary">Intekel</div>
    <!-- 404 Error Text -->
    <div class="text-center py-5">
        <div class="error mx-auto" data-text="404">404</div>
        <p class="lead text-gray-800">¡Pagina no encontrada!</p>
        <p class="text-gray-500 mb-5">Parece que encontraste un error en la matrix...</p>
        <a class="btn btn-primary" href="{{url('/inicio')}}">&larr; Regresar al inicio</a>
    </div>
</div>
@endsection