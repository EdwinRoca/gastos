@switch ($cfdiComprobante['FormaPago'])
    @case (01)
        <input type="text" class="form-control" name="tipopago" readonly value="Efectivo" required>
            @break
    @case (02)
        <input type="text" class="form-control" name="tipopago" readonly value="Cheque nominativo"required>
            @break
    @case (03)
        <input type="text" class="form-control" name="tipopago" readonly value="Transferencia electrónica de fondos" required>
            @break
    @case (04)
        <input type="text" class="form-control" name="tipopago" readonly value="Tarjeta de crédito" required>
            @break
    @case (05)
        <input type="text" class="form-control" name="tipopago" readonly value="Monedero electrónico" required>
            @break
    @case (06)
        <input type="text" class="form-control" name="tipopago" readonly value="Dinero electrónico" required>
            @break
    @case ('08')
        <input type="text" class="form-control" name="tipopago" readonly value="Vales de despensa" required>
            @break
    @case (12)
        <input type="text" class="form-control" name="tipopago" readonly value="Dación en pago" required>
            @break
    @case (13)
        <input type="text" class="form-control" name="tipopago" readonly value="Pago por subrogación" required>
            @break
    @case (14)
        <input type="text" class="form-control" name="tipopago" readonly value="Pago por consignación" required>
            @break
    @case (15)
        <input type="text" class="form-control" name="tipopago" readonly value="Condonación" required>
            @break
    @case (17)
        <input type="text" class="form-control" name="tipopago" readonly value="Compensación" required>
            @break
    @case (23)
        <input type="text" class="form-control" name="tipopago" readonly value="Novación" required>
            @break
    @case (24)
        <input type="text" class="form-control" name="tipopago" readonly value="Confusión" required>
            @break
    @case (25)
        <input type="text" class="form-control" name="tipopago" readonly value="Remisión de deuda" required>
            @break
    @case (26)
        <input type="text" class="form-control" name="tipopago" readonly value="Prescripción o caducidad" required>
            @break
    @case (27)
        <input type="text" class="form-control" name="tipopago" readonly value="A satisfacción del acreedor" required>
            @break
    @case (28)
        <input type="text" class="form-control" name="tipopago" readonly value="Tarjeta de débito" required>
            @break
    @case (29)
        <input type="text" class="form-control" name="tipopago" readonly value="Tarjeta de servicios" required>
            @break
    @case (30)
        <input type="text" class="form-control" name="tipopago" readonly value="Aplicación de anticipos" required>
            @break
    @case (99)
        <input type="text" class="form-control" name="tipopago" readonly value="Por definir" required>
            @break
    @default
        <input type="text" class="form-control" name="tipopago" readonly value="Por definir" required>
    @endswitch
