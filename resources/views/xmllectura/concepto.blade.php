<input type="text" class="form-control" name="comentario" readonly
    <?php 
    foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Conceptos//cfdi:Concepto') as $Concepto) {
        if (($Concepto['ClaveProdServ'] == "01010101")) {
            $value="No existe en el Catalogo";
        } elseif (($Concepto['ClaveProdServ']>=10000000) && ($Concepto['ClaveProdServ']<=10999999)) {
            $value="Materiales relacionados con la gauna, semillas y flora";
        } elseif (($Concepto['ClaveProdServ']>=11000000) && ($Concepto['ClaveProdServ']<=11999999)) {
            $value="Materiales de Minerales y Tejidos de Plantas y Animales no Comestibles";
        } elseif (($Concepto['ClaveProdServ']>=12000000) && ($Concepto['ClaveProdServ']<=12999999)) { 
            $value="Productos quimicos y Materiales Elastomericos";
        } elseif (($Concepto['ClaveProdServ']>=13000000) && ($Concepto['ClaveProdServ']<=13999999)) {
            $value="Resina y Colofonia y Caucho y Espuma y Película y Materiales Elastoméricos"; 
        } elseif (($Concepto['ClaveProdServ']>=14000000) && ($Concepto['ClaveProdServ']<=14999999)) {
            $value="Materiales y Productos de Papel";
        } elseif (($Concepto['ClaveProdServ']>=15000000) && ($Concepto['ClaveProdServ']<=15999999)) { 
            $value="Combustibles";
        } elseif (($Concepto['ClaveProdServ']>=20000000) && ($Concepto['ClaveProdServ']<=20999999)) { 
            $value="Maquinaria de minería y perforación de pozos y accesorios";      
        } elseif (($Concepto['ClaveProdServ']>=21000000) && ($Concepto['ClaveProdServ']<=21999999)) { 
            $value="Maquinaria y Accesorios para Agricultura"; 
        } elseif (($Concepto['ClaveProdServ']>=23000000) && ($Concepto['ClaveProdServ']<=23999999)) {
            $value="Maquinaria y Accesorios de Fabricación y Transformación Industrial"; 
        } elseif (($Concepto['ClaveProdServ']>=24000000) && ($Concepto['ClaveProdServ']<=24999999)) {  
            $value="Maquinaria y Accesorios de Embalaje y Contenedores";
        } elseif (($Concepto['ClaveProdServ']>=26000000) && ($Concepto['ClaveProdServ']<=26999999)) { 
            $value="Maquinaria y Accesorios para Generación y Distribución de Energía";
        } elseif (($Concepto['ClaveProdServ']>=27000000) && ($Concepto['ClaveProdServ']<=27999999)) { 
            $value="Herramientas y Maquinaria en General (equipo hidraúlico y neumático)";
        } elseif (($Concepto['ClaveProdServ']>=30000000) && ($Concepto['ClaveProdServ']<=30999999)) { 
            $value="Componentes y Suministros de Fabricación y Construcción";
        } elseif (($Concepto['ClaveProdServ']>=31000000) && ($Concepto['ClaveProdServ']<=31999999)) { 
            $value="Componentes y Suministros de Fabricación"; 
        } elseif (($Concepto['ClaveProdServ']>=32000000) && ($Concepto['ClaveProdServ']<=32999999)) { 
            $value="Componentes y Suministros Electrónicos";
        } elseif (($Concepto['ClaveProdServ']>=39000000) && ($Concepto['ClaveProdServ']<=39999999)) { 
            $value="Suministros de Iluminación y Electrónica";
        } elseif (($Concepto['ClaveProdServ']>=22000000) && ($Concepto['ClaveProdServ']<=22999999)) {
            $value="Maquinaria y Accesorios para Construcción y Edificación"; 
        } elseif (($Concepto['ClaveProdServ']>=25000000) && ($Concepto['ClaveProdServ']<=25999999)) {
            $value="Vehículos y Medios de Transportación"; 
        } elseif (($Concepto['ClaveProdServ']>=40000000) && ($Concepto['ClaveProdServ']<=40999999)) {  
            $value="Sistemas de calefacción, Tubería y Ventilación";
        } elseif (($Concepto['ClaveProdServ']>=41000000) && ($Concepto['ClaveProdServ']<=41999999)) {  
            $value="Equipo de Laboratorio";
        } elseif (($Concepto['ClaveProdServ']>=42000000) && ($Concepto['ClaveProdServ']<=42999999)) {  
            $value="Equipo Veterinario, Médicos, y Ortopédico"; 
        } elseif (($Concepto['ClaveProdServ']>=51000000) && ($Concepto['ClaveProdServ']<=51999999)) { 
            $value="Medicamentos y Productos Farmacéuticos";
        } elseif (($Concepto['ClaveProdServ']>=47000000) && ($Concepto['ClaveProdServ']<=47999999)) { 
            $value="Equipo y Suministros de limpieza";
        } elseif (($Concepto['ClaveProdServ']>=48000000) && ($Concepto['ClaveProdServ']<=48999999)) { 
            $value="Maquinaria y Equipos de cocina";
        } elseif (($Concepto['ClaveProdServ']>=50000000) && ($Concepto['ClaveProdServ']<=50999999)) { 
            $value="Alimentos";
        } elseif (($Concepto['ClaveProdServ']>=43000000) && ($Concepto['ClaveProdServ']<=43999999)) { 
            $value="Telecomunicaciones y radiodifusión de tecnología de la información"; 
        } elseif (($Concepto['ClaveProdServ']>=44000000) && ($Concepto['ClaveProdServ']<=44999999)) { 
            $value="Equipo";
        } elseif (($Concepto['ClaveProdServ']>=45000000) && ($Concepto['ClaveProdServ']<=45999999)) { 
            $value="Equipo y Suministros de Imprenta"; 
        } elseif (($Concepto['ClaveProdServ']>=55000000) && ($Concepto['ClaveProdServ']<=55999999)) { 
            $value="Productos Impresos";
        } elseif (($Concepto['ClaveProdServ']>=46000000) && ($Concepto['ClaveProdServ']<=46999999)) { 
            $value="Equipos y Suministros de Defensa"; 
        } elseif (($Concepto['ClaveProdServ']>=49000000) && ($Concepto['ClaveProdServ']<=49999999)) {
            $value="Equipos de deporte, accesorios y recreativos"; 
        } elseif (($Concepto['ClaveProdServ']>=52000000) && ($Concepto['ClaveProdServ']<=52999999)) { 
            $value="Muebles, Utensilios de cocina, Electrodomésticos y Accesorios para el hogar"; 
        } elseif (($Concepto['ClaveProdServ']>=53000000) && ($Concepto['ClaveProdServ']<=53999999)) { 
            $value="Ropa, calzado, maletas y artículos de tocador";
        } elseif (($Concepto['ClaveProdServ']>=54000000) && ($Concepto['ClaveProdServ']<=54999999)) { 
            $value="Productos para Relojería y Bisutería";
        } elseif (($Concepto['ClaveProdServ']>=56000000) && ($Concepto['ClaveProdServ']<=56999999)) {
            $value="Muebles y mobiliario";
        } elseif (($Concepto['ClaveProdServ']>=60000000) && ($Concepto['ClaveProdServ']<=60999999)) {
            $value="Productos de papelería escolares, musicales y juguetes"; 
        } elseif (($Concepto['ClaveProdServ']>=64000000) && ($Concepto['ClaveProdServ']<=64999999)) {
            $value="Contratos de seguro de salud";
        } elseif (($Concepto['ClaveProdServ']>=70000000) && ($Concepto['ClaveProdServ']<=70999999)) {
            $value="Servicios relacionados el sector primario"; 
        } elseif (($Concepto['ClaveProdServ']>=71000000) && ($Concepto['ClaveProdServ']<=71999999)) {
            $value="Servicios de Perforación de Minería";
        } elseif (($Concepto['ClaveProdServ']>=72000000) && ($Concepto['ClaveProdServ']<=72999999)) {
            $value="Servicios de Construcción y Mantenimiento"; 
        } elseif (($Concepto['ClaveProdServ']>=73000000) && ($Concepto['ClaveProdServ']<=73999999)) {
            $value="Servicios de Producción y Fabricación Industrial"; 
        } elseif (($Concepto['ClaveProdServ']>=76000000) && ($Concepto['ClaveProdServ']<=76999999)) {
            $value="Servicios de Limpieza Industrial";
        } elseif (($Concepto['ClaveProdServ']>=77000000) && ($Concepto['ClaveProdServ']<=77999999)) {
            $value="Servicios relacionados con el medio ambiente"; 
        } elseif (($Concepto['ClaveProdServ']>=78000000) && ($Concepto['ClaveProdServ']<=78999999)) {
            $value="Servicios de Transporte";
        } elseif (($Concepto['ClaveProdServ']>=80000000) && ($Concepto['ClaveProdServ']<=80999999)) {
            $value="Servicios de Gestión y Administrativo";
        } elseif (($Concepto['ClaveProdServ']>=81000000) && ($Concepto['ClaveProdServ']<=81999999)) {
            $value="Servicios basados en ingeniería";
        } elseif (($Concepto['ClaveProdServ']>=82000000) && ($Concepto['ClaveProdServ']<=82999999)) {
            $value="Servicios Editoriales y Publicidad";
        } elseif (($Concepto['ClaveProdServ']>=83000000) && ($Concepto['ClaveProdServ']<=83999999)) {
            $value="Servicios Públicos y Servicios Relacionados con el Sector Público";
        } elseif (($Concepto['ClaveProdServ']>=84000000) && ($Concepto['ClaveProdServ']<=84999999)) {
            $value="Servicios Financieros y de Seguros";
        } elseif (($Concepto['ClaveProdServ']>=85000000) && ($Concepto['ClaveProdServ']<=85999999)) {
            $value="Servicios Sanitarios y Hospitalarios";
        } elseif (($Concepto['ClaveProdServ']>=86000000) && ($Concepto['ClaveProdServ']<=86999999)) {
            $value="Servicios Educativos y de Formación";
        } elseif (($Concepto['ClaveProdServ']>=90000000) && ($Concepto['ClaveProdServ']<=90999999)) {
            $value="Servicios de Viajes y Alimentación";
        } elseif (($Concepto['ClaveProdServ']>=91000000) && ($Concepto['ClaveProdServ']<=91999999)) {  
            $value="Servicios Personales y Domésticos"; 
        } elseif (($Concepto['ClaveProdServ']>=92000000) && ($Concepto['ClaveProdServ']<=92999999)) { 
            $value="Servicios de Defensa Nacional";
        } elseif (($Concepto['ClaveProdServ']>=93000000) && ($Concepto['ClaveProdServ']<=93999999)) { 
            $value="Servicios Políticos y de Asuntos Cívicos";
        } elseif (($Concepto['ClaveProdServ']>=94000000) && ($Concepto['ClaveProdServ']<=94999999)) {
            $value="Organizaciones y Clubes";
        } else {
            $value = "No existe en el Catalogo";
        }
    }
    ?>                               value="{{$value}}" required>
