<input type="text" class="form-control" name="memo" readonly
    <?php 
    foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Conceptos//cfdi:Concepto') as $Concepto) {
        if (($Concepto['ClaveProdServ'] == "01010101")) {
            $value ="No existe en el Catalogo";
        } elseif ((($Concepto['ClaveProdServ']>=10000000) && ($Concepto['ClaveProdServ']<=10999999))
            || (($Concepto['ClaveProdServ']>=11000000) && ($Concepto['ClaveProdServ']<=11999999))
            || (($Concepto['ClaveProdServ']>=12000000) && ($Concepto['ClaveProdServ']<=12999999))
            || (($Concepto['ClaveProdServ']>=13000000) && ($Concepto['ClaveProdServ']<=13999999))
            || (($Concepto['ClaveProdServ']>=14000000) && ($Concepto['ClaveProdServ']<=14999999))
            || (($Concepto['ClaveProdServ']>=15000000) && ($Concepto['ClaveProdServ']<=15999999))) {
            $value = "Materias primas, químicos, papel y combustibles";
        } elseif ((($Concepto['ClaveProdServ']>=20000000) && ($Concepto['ClaveProdServ']<=20999999))
            || (($Concepto['ClaveProdServ']>=21000000) && ($Concepto['ClaveProdServ']<=21999999))
            || (($Concepto['ClaveProdServ']>=23000000) && ($Concepto['ClaveProdServ']<=23999999))
            || (($Concepto['ClaveProdServ']>=24000000) && ($Concepto['ClaveProdServ']<=24999999))
            || (($Concepto['ClaveProdServ']>=26000000) && ($Concepto['ClaveProdServ']<=26999999))
            || (($Concepto['ClaveProdServ']>=27000000) && ($Concepto['ClaveProdServ']<=27999999))) {
            $value = "Herramientas y equipos industriales";
        } elseif ((($Concepto['ClaveProdServ']>=30000000) && ($Concepto['ClaveProdServ']<=30999999))
            || (($Concepto['ClaveProdServ']>=31000000) && ($Concepto['ClaveProdServ']<=31999999))
            || (($Concepto['ClaveProdServ']>=32000000) && ($Concepto['ClaveProdServ']<=32999999))
            || (($Concepto['ClaveProdServ']>=39000000) && ($Concepto['ClaveProdServ']<=39999999))) {
            $value = "Suministros y componentes";
        } elseif ((($Concepto['ClaveProdServ']>=22000000) && ($Concepto['ClaveProdServ']<=22999999))
            || (($Concepto['ClaveProdServ']>=25000000) && ($Concepto['ClaveProdServ']<=25999999))
            || (($Concepto['ClaveProdServ']>=40000000) && ($Concepto['ClaveProdServ']<=40999999))) {
            $value = "Suministros y equipos de construcción, edificaciones y transportes";
        } elseif ((($Concepto['ClaveProdServ']>=41000000) && ($Concepto['ClaveProdServ']<=41999999))
            || (($Concepto['ClaveProdServ']>=42000000) && ($Concepto['ClaveProdServ']<=42999999))
            || (($Concepto['ClaveProdServ']>=51000000) && ($Concepto['ClaveProdServ']<=51999999))) {
            $value = "Productos farmacéuticos, y suministros y equipos de ensayo, de laboratorio y médicos";
        } elseif ((($Concepto['ClaveProdServ']>=43000000) && ($Concepto['ClaveProdServ']<=43999999))
            || (($Concepto['ClaveProdServ']>=44000000) && ($Concepto['ClaveProdServ']<=44999999))
            || (($Concepto['ClaveProdServ']>=45000000) && ($Concepto['ClaveProdServ']<=45999999))
            || (($Concepto['ClaveProdServ']>=55000000) && ($Concepto['ClaveProdServ']<=55999999))) {
            $value = "Suministros y equipos tecnológicos, de comunicaciones y de negocios";    
        } elseif (($Concepto['ClaveProdServ']>=46000000) && ($Concepto['ClaveProdServ']<=46999999)) {
            $value = "Suministros y equipos de defensa y seguridad";
        } elseif ((($Concepto['ClaveProdServ']>=49000000) && ($Concepto['ClaveProdServ']<=49999999))
            || (($Concepto['ClaveProdServ']>=52000000) && ($Concepto['ClaveProdServ']<=52999999))
            || (($Concepto['ClaveProdServ']>=53000000) && ($Concepto['ClaveProdServ']<=53999999))
            || (($Concepto['ClaveProdServ']>=54000000) && ($Concepto['ClaveProdServ']<=54999999))
            || (($Concepto['ClaveProdServ']>=56000000) && ($Concepto['ClaveProdServ']<=56999999))
            || (($Concepto['ClaveProdServ']>=60000000) && ($Concepto['ClaveProdServ']<=60999999))) {
            $value = "Suministros y equipos de consumo, domésticos y personales";
        } elseif ((($Concepto['ClaveProdServ']>=64000000) && ($Concepto['ClaveProdServ']<=64999999))
            || (($Concepto['ClaveProdServ']>=70000000) && ($Concepto['ClaveProdServ']<=70999999))
            || (($Concepto['ClaveProdServ']>=71000000) && ($Concepto['ClaveProdServ']<=71999999))
            || (($Concepto['ClaveProdServ']>=72000000) && ($Concepto['ClaveProdServ']<=72999999))
            || (($Concepto['ClaveProdServ']>=73000000) && ($Concepto['ClaveProdServ']<=73999999))
            || (($Concepto['ClaveProdServ']>=76000000) && ($Concepto['ClaveProdServ']<=76999999))
            || (($Concepto['ClaveProdServ']>=77000000) && ($Concepto['ClaveProdServ']<=77999999))
            || (($Concepto['ClaveProdServ']>=78000000) && ($Concepto['ClaveProdServ']<=78999999))
            || (($Concepto['ClaveProdServ']>=80000000) && ($Concepto['ClaveProdServ']<=80999999))
            || (($Concepto['ClaveProdServ']>=81000000) && ($Concepto['ClaveProdServ']<=81999999))
            || (($Concepto['ClaveProdServ']>=82000000) && ($Concepto['ClaveProdServ']<=82999999))
            || (($Concepto['ClaveProdServ']>=83000000) && ($Concepto['ClaveProdServ']<=83999999))
            || (($Concepto['ClaveProdServ']>=84000000) && ($Concepto['ClaveProdServ']<=84999999))
            || (($Concepto['ClaveProdServ']>=85000000) && ($Concepto['ClaveProdServ']<=85999999))
            || (($Concepto['ClaveProdServ']>=86000000) && ($Concepto['ClaveProdServ']<=86999999))
            || (($Concepto['ClaveProdServ']>=90000000) && ($Concepto['ClaveProdServ']<=90999999))
            || (($Concepto['ClaveProdServ']>=91000000) && ($Concepto['ClaveProdServ']<=91999999))
            || (($Concepto['ClaveProdServ']>=92000000) && ($Concepto['ClaveProdServ']<=92999999))
            || (($Concepto['ClaveProdServ']>=93000000) && ($Concepto['ClaveProdServ']<=93999999))
            || (($Concepto['ClaveProdServ']>=94000000) && ($Concepto['ClaveProdServ']<=94999999))) {
            $value = "Servicios";
        } else {
            $value = "No existe en el Catalogo";
        }

    }
    ?>
                                value="{{$value}}" required>
