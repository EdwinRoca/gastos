@extends('template')

@section('title','Generar Gastos')

@section('header')
@if (session('success'))
<div class="sufee-alert alert with-close alert-success alert-dismissible fade show" role="alert">
    {{ session()->get('success') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<a href="{{url('/gastos')}}" class="btn btn-sm btn-danger">
<i class="fa fa-arrow-left"></i> Regresar</a>

@endsection

@section('content')
<?php
/**
 * PHP Version 7.2.10
 * Cargar Archivo xml
 * 
 * @category Blade
 * @package  Resources\Views
 * @author   Carlos <carlosflres095@gmail.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
 * @link     https://yoursite.com
 */
$xml = simplexml_load_file("storage/xmlfile.xml"); 

$ns = $xml->getNamespaces(true);

$xml->registerXPathNamespace('c', $ns['cfdi']);
$xml->registerXPathNamespace('t', $ns['tfd']);

?>

<div class="row">
    <div class="col-lg-8 mx-auto">
        <div class="card shadow">
            <div class="card-header bg-info text-center">
                <h1 class="text-white mb-0">Formulario de gastos</h1>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col">

                        <form action="{{ url('generar_gastoxml/crear')}}" 
                                enctype="multipart/form-data" method="post">
                        {!! csrf_field() !!} 
                               
                        <div class="row">   
                            <div class="form-group col-md-6">
                            <label class="text-info" for="concepto">
                                Concepto *
                            </label>
                                <input type="text" class="form-control" 
                                        name="concepto"
                                    <?php
                                    foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Conceptos//cfdi:Concepto') as $Concepto) {
                                        $value = "{$Concepto['Descripcion']}";
                                    }
                                    ?>
                                        value="{{$value}}"
                                        readonly required>
                            </div>                     
                        </div>

                        <div class="row">
                            <div class="form-group col-md-4">
                                <label class="text-info" for="memo">
                                    Memo *
                                </label>
                            @include('xmllectura.motivos')
                            </div>
                    @foreach ($xml->xpath('//cfdi:Comprobante') as $cfdiComprobante)
                            <div class="form-group col-md-4">
                                <label class="text-info" for="tipo_pago">
                                    Tipo de pago *
                                </label>
                            @include('xmllectura.formadepago')
                        </div>
                    @endforeach
                    @foreach ($xml->xpath('//cfdi:Comprobante') as $cfdiComprobante)
                    @if (isset($cfdiComprobante['Pais'])) 
                        <div class="form-group col-md-4">
                            <label class="text-info" for="pais">Pais *</label>
                                <input type="text" class="form-control" 
                                        name="pais" 
                                        value="$cfdiComprobante['Pais']" readonly
                                        required>
                        </div>   
                    @else
                        <div class="form-group col-md-4">
                            <label class="text-info" for="pais">Pais *</label>
                                <input type="text" class="form-control" 
                                        name="pais" 
                                        value="MEX" readonly
                                        required>
                        </div>
                    @endif
                    @endforeach
                        <div class="form-group col-md-8">
                        <label class="text-info" for="comentario">
                                    Comentario *
                                </label>
                            @include('xmllectura.concepto')
                        </div>
                    @foreach ($xml->xpath('//cfdi:Comprobante') as $cfdiComprobante)
                        <?php
                            $date = date_create($cfdiComprobante['Fecha']); 
                            $date = date_format($date, 'Y-m-d');
                        ?>      
                            <div class="form-group col-md-4">
                                <label class="text-info" for="fecha">
                                    Fecha *
                                </label>
                                    <input type="date" class="form-control" 
                                            name="fecha" 
                                            value="{{$date}}" readonly
                                            required>
                                </div>
                    @endforeach

                    @foreach ($xml->xpath('//cfdi:Comprobante') as $cfdiComprobante) 
                    
                    @if (isset($cfdiComprobante['TipoCambio']))
                        <div class="form-group col-md-4">
                            <label class="text-info" for="orden_servicio">
                            Tipo de cambio *
                            </label>
                                <input type="text" class="form-control" 
                                        value="{{$cfdiComprobante['TipoCambio']}}"
                                        name="tipocambio" readonly
                                        required>
                        </div>
                    @else
                            <div class="form-group col-md-4">
                            <label class="text-info" for="orden_servicio">
                            Tipo de cambio *
                            </label>
                                <input type="text" class="form-control" 
                                        value="1"
                                        name="tipocambio" readonly
                                        required>
                            </div>
                    @endif
                    @endforeach

                    @foreach ($xml->xpath('//cfdi:Comprobante') as $cfdiComprobante)

                            <div class="form-group col-md-4">
                                <label class="text-info" for="moneda">
                                    Moneda *
                                </label>
                                <input type="text" class="form-control" 
                                        name="moneda" readonly
                                        value="{{$cfdiComprobante['Moneda']}}"
                                        required>
                            </div>
                        </div>
                            <div class="row d-flex justify-content-end mt-5">
                            <div class="form-group col-md-4 ">
                                <label class="text-info" for="monto">
                                    Monto Total *
                                </label>
                                <div class="input-group">
                                <div class="input-group-prepend">
                                        <span class="input-group-text">$</span>
                                        </div>
                                    <input type="text" class="form-control " 
                                                name="monto" readonly
                                                value="{{$cfdiComprobante['Total']}}"
                                                required>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            {{$cfdiComprobante['Moneda']}}
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    @endforeach
                            <input type="hidden" name="status" value="proceso">
                            <input type="hidden" name="usuarioid" value="{{ auth()->user()->id }}">
                            <input type="hidden" name="empresaid" value="{{ auth()->user()->empresas_id}}">
                            
                            <div class="card-footer">
                            <div class="row">
                                <div class="col-md-8">
                                <p>Los campos marcados con asteriscos son obligatorios</p>
                            </div>
                            <div class="col-md-4">
                                <button type="submit" 
                                        class="btn  btn-info float-right">
                                    Crear
                                </button>
                            </div>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection