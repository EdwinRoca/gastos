@extends('template')

@section('title','Cargar Archivo')

@section('header')
@if (session('filealert'))
  <div class="alert alert-danger alert-dismissible fade show" role="alert">
    {{ session()->get('filealert') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
@if (session('success'))
  <div class="sufee-alert alert with-close alert-success alert-dismissible fade show" role="alert">
    {{ session()->get('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
<a href="{{url('/gastos')}}" class="btn btn-sm btn-danger">
<i class="fa fa-arrow-left"></i> Regresar</a>
@endsection

@section('content')
<div class="row">
  <div class="col-lg-8 mx-auto">
    <div class="card shadow">
      
      <div class="card-header bg-info text-center">
        <h1 class="text-white mb-0">Nuevo Archivo</h1>
      </div>
      
      <div class="card-body">
        <div class="row">
          <div class="col">

            <form method="POST" action="/upload/check" accept-charset="UTF-8" enctype="multipart/form-data">
              
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              

                <div class="form-group">
                  <input type="file" name="file" accept=".xml">
                </div>
              

              <div class="form-group">
                <div class="form-group mb-0">
                  <button type="submit" class="btn btn-primary">Enviar</button>
                </div>
              </div>

            </form>
          
          </div> 
        </div>
      </div>
    </div>
  </div>
</div>
@endsection