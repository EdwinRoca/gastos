@extends('template')

@section('title','Lista de usuarios')

@section('header')
<a href="{{url('/usuarios')}}" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left"></i> Regresar</a>
@endsection

@section('content')
<div class="row mt-4">
    <div class="col-lg mx-auto">
        <div class="card shadow border-bottom-primary">
            <div class="card-body table-responsive">
                <table id="usuarios" class="table align-items-center table-hover table-borderless table-flush">
                    <thead class="thead-light">
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Empresa</th>
                        <th>Departamento</th>
                        <th>Categoría</th>
                        <th>Estado</th>
                    </thead>
                    <tbody>
                    @foreach($usuarios as $usuario)
                        <tr>
                            <td>{{$usuario->id}}</td>
                            <td>{{$usuario->nombre}} <span class="badge badge-pill badge-primary">{{$usuario->nombre == auth()->user()->nombre ? 'Tú': ''}}</span></td>
                            <td>{{$usuario->email}}</td>
                            <td>{{$usuario->empresa->nombre}}</td>
                            <td>{{$usuario->departamento == '1' ? 'Ventas' : '' }}{{$usuario->departamento == '2' ? 'Sistemas' : ''}}</td>
                            <td class="{{$usuario->categorias_id == '1' ? 'text-danger' : 'text-success'}}">{{$usuario->categorias_id == '1' ? 'Supervisor' : '' }}{{$usuario->categorias_id == '2' ? 'Empleado' : ''}}</td>
                            <td><span class="badge badge-pill {{ $usuario->estatus == 'Activo' ? 'badge-success' : 'badge-danger' }}">{{$usuario->estatus}}</span></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection