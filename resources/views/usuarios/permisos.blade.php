@extends('template')

@section('title','Editar permisos')

@section('header')
<a href="{{url('/usuarios/permisos')}}" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left"></i> Regresar</a>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-9 mb-4 mx-auto">
        <div class="card shadow">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <h3 class="text-primary">Nombre de usuario:</h3>
                        <p>Henry Ford</p>
                        <h3 class="text-primary">Empresa:</h3>
                        <p>Ford</p>
                        <h3 class="text-primary">Categoría:</h3>
                        <p>Empleado</p>
                    </div>
                    <div class="col-6">
                        <h2 class="text-center text-primary">Permisos del usuario</h2>
                        <form action="" method="post">
                        <div class="card bg-gray-100">
                            <div class="card-body">
                                <div class="custom-control custom-control-alternative custom-checkbox mb-3">
                                    <input class="custom-control-input" id="P1" type="checkbox" checked>
                                    <label class="custom-control-label" for="P1">Permiso 1</label>
                                </div>
                                <div class="custom-control custom-control-alternative custom-checkbox mb-3">
                                    <input class="custom-control-input" id="P2" type="checkbox">
                                    <label class="custom-control-label" for="P2">Permiso 2</label>
                                </div>
                                <div class="custom-control custom-control-alternative custom-checkbox mb-3">
                                    <input class="custom-control-input" id="P3" type="checkbox" checked>
                                    <label class="custom-control-label" for="P3">Permiso 3</label>
                                </div>
                                <div class="custom-control custom-control-alternative custom-checkbox mb-3">
                                    <input class="custom-control-input" id="P4" type="checkbox">
                                    <label class="custom-control-label" for="P4">Permiso 4</label>
                                </div>
                            </div>
                        </div>
                        <div class="text-right mt-3"><button type="submit" class="btn btn-primary">Guardar cambios</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection