@extends('template')

@section('title','Actualizar Usuario')

@section('header')
<a href="{{url('/usuarios/actualizar')}}" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left"></i> Regresar</a>
@endsection

@section('content')

<div class="row">
    <div class="col-lg-8 mx-auto">
        <div class="card shadow">
            <div class="card-header bg-info text-center">
                <h1 class="text-white mb-0">Actualizar usuario</h1>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col">

                        <form action="{{ action('ActualizarController@update', $usuarios['id'])}}" method="post">
                        {!! csrf_field() !!} 
                        @method('PATCH')

                            <div class="form-group">
                            <label class="text-info" for="nombre">Nombre:</label>
                                <input type="text" class="form-control" 
                                        placeholder="Nombre" name="nombre"
                                        pattern="[A-Z a-z]+" value="{{ $usuarios['nombre'] }}" required>
                            </div>

                            <div class="form-group">
                            <label class="text-info" for="email">Correo electrónico:</label>
                                <input type="email" class="form-control"
                                        name="email" placeholder="Correo electrónico" value="{{ $usuarios['email'] }}"
                                        required>
                            </div>
                            <!-- <div class="form-group">
                            <label class="text-info" for="banco">Información Bancaria:</label>
                                <input type="text" class="form-control"
                                        name="banco" placeholder="Banco"  value="{{ $usuarios['banco'] }}" 
                                        required>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" 
                                                name="cuenta" pattern="[0-9]+"
                                                placeholder="Cuenta" value="{{ $usuarios['cuenta'] }}" required>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" 
                                                name="clabe" pattern="[0-9]{18}" 
                                                placeholder="Clabe (18 digitos)" value="{{ $usuarios['clabe'] }}" required>
                                    </div>
                                </div>
                            </div> -->
                            <div class="form-group">
                            <label class="text-info" for="departamento">Departamento:</label>
                                <select name="departamento" id="departamentos" class="form-control" value="{{ $usuarios['departamento'] }}" required>
                                <option value="" >Selecciona un departamento </option>
                                <option value="1" {{($usuarios['departamento'] == 1) ? 'selected': '' }}>Ventas</option>
                                <option value="2" {{($usuarios['departamento'] == 2) ? 'selected': '' }}>Sistemas</option>
                                </select>
                            </div>

                            <div class="form-group">
                            <label class="text-info" for="orden_servicio">Orden de Servicio:</label>
                                <input type="number" class="form-control" 
                                        name="orden_servicio" 
                                        placeholder="Ingrese Numero de Orden" value="{{ $usuarios['orden_servicio'] }}" required>
                            </div>

                            <div class="form-group">
                            <label class="text-info" for="estatus">Estatus:</label><br>
                            <input type="radio" name="estatus" value="Activo" {{ $usuarios['estatus'] == 'Activo' ? 'checked' : ''}} >Activo
                            <input type="radio" name="estatus" value="Inactivo" {{ $usuarios['estatus'] == 'Inactivo' ? 'checked' : ''}} >Inactivo
                            </div>
                            
                            <div class="form-group">
                            <label class="text-info" for="empresa">Empresa:</label>
                                <select name="empresas_id" id="empresas_id" 
                                        class="form-control" placeholder="Empresa" value="{{ $usuarios['empresas_id'] }}" required>
                                        <option value="">Selecciona una Empresa </option>
                                        @foreach ($empresas as $emp)
                                            <option value="{{$emp->id}}" {{($usuarios['empresas_id'] == $emp->id) ? 'selected': '' }}>{{ $emp->nombre }} </option>
                                        @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                            <label class="text-info" for="categorias_id">Categoría:</label>
                                <select name="categorias_id" id="categorias_id" class="form-control" placeholder="Categoría" value="{{ $usuarios['categorias_id'] }}" required>
                                    <option value="">Selecciona Categoria</option>
                                    <option value="1" {{($usuarios['categorias_id'] == 1) ? 'selected': '' }} >Supervisor</option>
                                    <option value="2" {{($usuarios['categorias_id'] == 2) ? 'selected': '' }} >Empleado</option>
                                </select>
                            </div>

                            <!-- <div class="form-group">
                            <label class="text-info" for="password">Contraseña:</label>
                                <input type="password" name="password"
                                        class="form-control" placeholder="Contraseña">
                            </div> -->

                            <div class="form-group mb-0">
                                <button type="submit" class="btn btn-block btn-info">Actualizar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection