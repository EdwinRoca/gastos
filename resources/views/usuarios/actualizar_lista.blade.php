@extends('template')

@section('title','Actualizar usuarios')

@section('header')
@if (session('success'))
    <div class="sufee-alert alert with-close alert-success alert-dismissible fade show" role="alert">
        {{ session()->get('success') }}	
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
<a href="{{url('/usuarios')}}" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left"></i> Regresar</a>
@endsection

@section('content')
<div class="row mt-4">
    <div class="col-lg">
        <div class="card border-bottom-info shadow">
            <div class="card-body table-responsive">
                <table id="usuarios" class="table align-items-center table-hover table-borderless">
                    <thead class="thead-light">
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Empresa</th>
                        <th>Departamento</th>
                        <th>Categoría</th>
                        <th></th>
                    </thead>
                    <tbody>
                    @foreach($usuarios as $usuario)
                    <tr>
                            <td>{{$usuario->id}}</td>
                            <td>{{$usuario->nombre}} <span class="badge badge-pill badge-primary">{{$usuario->nombre == auth()->user()->nombre ? 'Tú': ''}}</span></td>
                            <td>{{$usuario->email}}</td>
                            <td>{{$usuario->empresa->nombre}}</td>
                            <td>{{($usuario->departamento == 1) ? 'Ventas' : ''}}{{($usuario->departamento == 2) ? 'Sistemas' : ''}}</td>
                            <td>{{($usuario->categorias_id == 1) ? 'Supervisor' : ''}}{{($usuario->categorias_id == 2) ? 'Empleado' : ''}}</td>
                            <td><a href="{{action('ActualizarController@edit', $usuario->id)}}" class="btn btn-info btn-sm">Actualizar</a></td>
                        </tr>
                    @endforeach
                        <!-- <tr>
                            <td>2</td>
                            <td>Nickola Tesla</td>
                            <td>Nickola@email.com</td>
                            <td>Tesla Motors</td>
                            <td>Engineering</td>
                            <td class="text-danger">Supervisor</td>
                            <td><a href="{{url('usuarios/actualizar/id')}}" class="btn btn-info btn-sm">Actualizar</a></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Henry Ford</td>
                            <td>HFord@email.com</td>
                            <td>Ford</td>
                            <td>Ejecutivo</td>
                            <td class="text-danger">Supervisor</td>
                            <td><a href="{{url('usuarios/actualizar/id')}}" class="btn btn-info btn-sm">Actualizar</a></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Linus Torvalds</td>
                            <td>Ubuntu@email.com</td>
                            <td>Linux</td>
                            <td>Developing</td>
                            <td class="text-success">Empleado</td>
                            <td><a href="{{url('usuarios/actualizar/id')}}" class="btn btn-info btn-sm">Actualizar</a></td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Steve Wozniak</td>
                            <td>SteWoz@email.com</td>
                            <td>Apple</td>
                            <td>Enginering</td>
                            <td class="text-success">Empleado</td>
                            <td><a href="{{url('usuarios/actualizar/id')}}" class="btn btn-info btn-sm">Actualizar</a></td>
                        </tr> -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection