@extends('template')

@section('title','Usuarios activos')

@section('header')
@if (session('success'))
    <div class="sufee-alert alert with-close alert-success alert-dismissible fade show" role="alert">
        {{ session()->get('success') }}	
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
<a href="{{url('/usuarios')}}" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left"></i> Regresar</a>
@endsection

@section('content')
<div class="row mt-4">
    <div class="col-lg mx-auto">
        <div class="card border-bottom-danger shadow">
            <div class="card-body table-responsive">
                <table id="usuarios" class="table align-items-center table-hover table-borderless table-flush">
                    <thead class="thead-light">
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Empresa</th>
                        <th>Departamento</th>
                        <th>Categoría</th>
                        <th>Estado</th>
                        <th></th>
                    </thead>
                    <tbody>
                    @foreach($usuarios as $usuario)
                        <tr>
                            <td>{{$usuario->id}}</td>
                            <td>{{$usuario->nombre}} <span class="badge badge-pill badge-primary">{{$usuario->nombre == auth()->user()->nombre ? 'Tú': ''}}</span></td>
                            <td>{{$usuario->empresa->nombre}}</td>
                            <td>{{$usuario->departamento == '1' ? 'Ventas' : '' }}{{$usuario->departamento == '2' ? 'Sistemas' : ''}}</td>
                            @if($usuario->categorias_id == 1)
                            <td class="text-danger">{{$usuario->categorias_id == 1 ? 'Supervisor' : ''}}</td>
                            @elseif($usuario->categorias_id == 2)
                            <td class="text-success">{{$usuario->categorias_id == 2 ? 'Empleado' : ''}}</td>
                            @endif
                            <td><span class="badge badge-pill {{ $usuario->estatus == 'Activo' ? 'badge-success' : 'badge-danger' }}">{{$usuario->estatus}}</span></td>
                            <td>
                                @if($usuario->estatus == "Activo")
                                @if($usuario->nombre !== auth()->user()->nombre)
                                <a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#modal-bloquear-{{$usuario->id}}">Bloquear</a>
                                @endif
                                <!-- Modal Bloquear -->
                                <div class="modal fade" id="modal-bloquear-{{$usuario->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-activar" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header bg-danger">
                                    <h3 class="text-white mb-0">Bloquear usuario</h3>
                                    </div>
                                    <div class="modal-body">
                                        ¿está seguro que desea <strong class="text-danger">BLOQUEAR</strong> la cuenta de este usuario?
                                        <div class="alert alert-danger mb-0 mt-2">
                                        <p class="mb-0"><strong>Usuario: </strong> {{$usuario->nombre}}</p>
                                        <p class="mb-0" ><strong>Empresa: </strong> {{$usuario->empresa->nombre}}</p>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                                        <form action="{{action('ActivarUsuarioController@update', $usuario->id)}}" method="post">
                                            {{ csrf_field() }}
                                            @method('PATCH')
                                            <button type="submit" class="btn btn-danger">Bloquear</a>
                                        </form>
                                    </div>
                                    </div>
                                </div>
                                </div>
                                @else
                                <a href="#" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal-activar-{{$usuario->id}}">Activar</a>
                                <!-- Modal Activar -->
                                <div class="modal fade" id="modal-activar-{{$usuario->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-activar" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header bg-success">
                                    <h3 class="text-white mb-0">Activar usuario</h3>
                                    </div>
                                    <div class="modal-body">
                                        ¿está seguro que desea <strong class="text-success">ACTIVAR</strong> la cuenta de este usuario?
                                        <div class="alert alert-success mb-0 mt-2">
                                        <p class="mb-0"><strong>Usuario: </strong> {{$usuario->nombre}}</p>
                                        <p class="mb-0" ><strong>Empresa: </strong> {{$usuario->empresa->nombre}}</p>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                                        <form action="{{action('ActivarUsuarioController@update', $usuario->id)}}" method="post">
                                            {{ csrf_field() }}
                                            @method('PATCH')
                                            <button type="submit" class="btn btn-success">Activar</a>
                                        </form>
                                    </div>
                                    </div>
                                </div>
                                </div>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection