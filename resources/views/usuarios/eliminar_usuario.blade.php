@extends('template')

@section('title','Eliminar usuarios')

@section('header')
@if (session('success'))
    <div class="sufee-alert alert with-close alert-success alert-dismissible fade show" role="alert">
        {{ session()->get('success') }}	
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
<a href="{{url('/usuarios')}}" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left"></i> Regresar</a>
@endsection

@section('content')
<div class="row mt-4">
    <div class="col-lg">
        <div class="card border-bottom-info shadow">
            <div class="card-body table-responsive">
                <table id="usuarios" class="table align-items-center table-hover table-borderless">
                    <thead class="thead-light">
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Empresa</th>
                        <th>Departamento</th>
                        <th>Categoría</th>
                        <th></th>
                    </thead>
                    <tbody>
                    @foreach($usuarios as $usuario)
                    <tr>
                            <td>{{$usuario->id}}</td>
                            <td>{{$usuario->nombre}} <span class="badge badge-pill badge-primary">{{$usuario->nombre == auth()->user()->nombre ? 'Tú': ''}}</span> </td>
                            <td>{{$usuario->email}}</td>
                            <td>{{$usuario->empresa->nombre}}</td>
                            <td>{{($usuario->departamento == 1) ? 'Ventas' : ''}}{{($usuario->departamento == 2) ? 'Sistemas' : ''}}</td>
                            <!-- <td>{{($usuario->categorias_id == 1) ? 'Supervisor' : ''}}{{($usuario->categorias_id == 2) ? 'Empleado' : ''}}</td> -->
                            @if($usuario->categorias_id == 1)
                            <td class="text-danger">{{$usuario->categorias_id == 1 ? 'Supervisor' : ''}}</td>
                            @elseif($usuario->categorias_id == 2)
                            <td class="text-success">{{$usuario->categorias_id == 2 ? 'Empleado' : ''}}</td>
                            @endif
                            <!-- <td><a href="{{action('EliminarController@destroy', $usuario->id)}}" class="btn btn-info btn-danger">Eliminar</a></td>
                            @csrf
                            @method('DELETE') -->
                            <td>
                                @if($usuario->nombre !== auth()->user()->nombre)
                                <a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#modal-bloquear-{{$usuario->id}}">Eliminar</a>
                                @endif
                                <!-- Modal Eliminar -->
                                <div class="modal fade" id="modal-bloquear-{{$usuario->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-activar" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header bg-danger">
                                    <h3 class="text-white mb-0">Eliminar usuario</h3>
                                    </div>
                                    <div class="modal-body">
                                        ¿Está seguro que desea <strong class="text-danger">ELIMINAR</strong> la cuenta de este usuario?
                                       
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                                        <form action="{{action('EliminarController@destroy', $usuario->id)}}" method="post">
                                            {{ csrf_field() }}
                                            <input name="_method" type="hidden" value="DELETE">
                                            <button type="submit" class="btn btn-danger">Eliminar</a>
                                        </form>
                                    </div>
                                    </div>
                                </div>
                                </div>
                            <!-- <form action="{{action('EliminarController@destroy', $usuario->id)}}" method="post">
                            {{csrf_field()}}
                            <input name="_method" type="hidden" value="DELETE">

                            <button class="btn btn-danger btn-xs" type="submit"><span class="glyphicon glyphicon-trash"></span>Eliminar</button>
                            </form>
                            </td> -->
                    </tr>
                            @endforeach
                    
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection