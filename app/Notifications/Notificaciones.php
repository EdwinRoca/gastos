<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class Notificaciones extends Notification
{
    use Queueable;

    /**
     * Los datos para la notificación
     * 
     * @var string
     */
    protected $data;

    protected $via;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data, $via="database")
    {
        $this->data = $data;
        $this->via = $via;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $this->via;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Se ha generado un nuevo gasto')
            ->greeting('Hola ' . $notifiable->name)
            ->line('Recibes este email porque se ha generado un nuevo gasto.')
            // ->action('Restablecer contraseña', url(config('app.url').route('password_reset', $this->token, false)))
            // ->line('Si no realizaste esta petición puedes ignorar este correo')
            ->salutation('Saludos');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable) 
    {
        return ['datos' => $this->data];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
