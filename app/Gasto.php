<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\GastosController;

class Gasto extends Model
{
    protected $fillable = ['usuario_id', 'empresa_id', 'archivo', 'concepto', 'memo',
     'tipo_pago', 'pais', 'comentario', 'fecha', 'tipo_cambio', 'moneda', 'monto',
      'estatus'];
    protected $table = 'gastos';

    public function empresa()
    {
        return $this->belongsTo(Empresa::class, 'empresa_id');
    }

    /**
     * Relacion usuario categoria
     * 
     * @return .
     */
    public function usuario()
    {
        return $this->belongsTo(Usuario::class, 'usuario_id');
    }
}
