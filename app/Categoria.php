<?php
/**
 * PHP Version 7.2.10
 * 
 * @category Model
 * @package  App
 * @author   Carlos <carlosflres095@gmail.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
 * @link     https://yoursite.com
 */
namespace App;

use App\CategoriaController;
use Illuminate\Database\Eloquent\Model;
/**
 * Asd
 * 
 * @category Model
 * @package  App
 * @author   Carlos <carlosflres095@gmail.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
 * @link     https://yoursite.com
 */
class Categoria extends Model
{
    protected $guarded = [];
    protected $table = 'categorias';
    protected $primarykey = 'id';

    /**
     * Relacion usuario categoria
     * 
     * @return .
     */
    public function usuario()
    {
        return $this->hasOne(Usuario::class, 'categorias_id');
    }
}
