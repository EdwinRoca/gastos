<?php
/**
 * PHP Version 7.2.10
 * 
 * @category Model
 * @package  App
 * @author   Carlos <carlosflres095@gmail.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
 * @link     https://yoursite.com
 */
namespace App;

use Illuminate\Database\Eloquent\Model;
use App\EmpresaController;
/**
 * Asd
 * 
 * @category Model
 * @package  App
 * @author   Carlos <carlosflres095@gmail.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
 * @link     https://yoursite.com
 */
class Empresa extends Model
{
    protected $guarded = [];
    protected $table = 'empresas';
    protected $primarykey = 'id';

    /**
     * Relacion usuario empresa
     * 
     * @return .
     */
    public function usuario()
    {
        return $this->hasOne(Usuario::class, 'empresas_id');
    }
}
