<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Notifications\Notificaciones;
use Illuminate\Support\Facades\Auth;
use App\Gasto;
use Validator;
class GastosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:usuarios');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gastos = Gasto::all();
        return view('gastos.index_gastos', compact('gastos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('gastos.form_gastos');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $gastos = new Gasto;

        $v = \Validator::make(
            $request->all(), [
            'memo' => 'required',
            'tipopago' => 'required',
            'pais' => 'required',
            'comentario' => 'required',
            'fecha' => 'required',
            'tipocambio' => 'required',
            'moneda' => 'required',
            'monto' => 'required',
            'status' => 'required',
            'usuarioid' =>'required',
            'empresaid' => 'required',
            
            ]
        );

        $user = Auth::user(); 
        if ($v->fails()) {
            $user->notify(new Notificaciones('El formulario de gastos ha quedado incompleto, puedes terminar de llenarlo más adelante.'));
            return redirect()->back()->withInput()->withErrors($v->errors());
        }

        //$usuario->create($request->all());
        $gastos->usuario_id = $request->input('usuarioid');
        $gastos->empresa_id = $request->input('empresaid');
        $gastos->memo = $request->input('memo');
        $gastos->tipo_pago = $request->input('tipopago');
        $gastos->pais = $request->input('pais');
        $gastos->comentario = $request->input('comentario');
        $gastos->fecha = $request->input('fecha');
        $gastos->tipo_cambio = $request->input('tipocambio');
        $gastos->moneda = $request->input('moneda');
        $gastos->monto = $request->input('monto');
        $gastos->estatus = $request->input('status');
        $gastos->save();
        $gastos = Gasto::all();

        $user->email = "erick.95.10@hotmail.com";
        $user->notify(new Notificaciones('', 'mail'));

        return redirect('/generar_gasto')
                ->with('success', 'Gasto Generado');
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_2(Request $request)
    {
        $gastos = (new Gasto)->fill($request->all());

        $validator = \Validator::make(
            $request->all(), [

            'archivo'=> 'required|mimes:png,jpeg,jpg,pdf',
            'concepto' => 'required',
            'status' => 'required',
            'usuarioid' =>'required',
            'empresaid' => 'required',
            
            ]
        );

        $user = Auth::user(); 
        $v = \Validator::make(
            $request->all(), [

            'archivo'=> 'required|mimes:png,jpeg,jpg,pdf',
            'concepto' => 'required',
            'status' => 'required',
            'usuarioid' =>'required',
            'empresaid' => 'required',
            
            ]
        );

        if ($v->fails()) {
            $user->notify(new Notificaciones('El formulario de gastos ha quedado incompleto, puedes terminar de llenarlo más adelante.'));
            return redirect()->back()->withInput()->withErrors($v->errors());
        }

        //$usuario->create($request->all());
        $gastos->usuario_id = $request->input('usuarioid');
        $gastos->empresa_id = $request->input('empresaid');
        if ($request->hasFile('archivo')) {
            $gastos->archivo = $request->file('archivo')->store('archivos'); 
        }
        $gastos->concepto = $request->input('concepto');
        $gastos->estatus = $request->input('status');
        $gastos->save();
        $gastos = Gasto::all();
        
        $user->email = "erick.95.10@hotmail.com";
        $user->notify(new Notificaciones('', 'mail'));
        return redirect('/generar_gasto')
                ->with('success', 'Gasto Generado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



}
