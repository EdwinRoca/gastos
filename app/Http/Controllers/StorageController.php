<?php

/**
 * PHP Version 7.2.10
 * 
 * @category Controller
 * @package  App\Http\Controllers
 * @author   Carlos <carlosflres095@gmail.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
 * @link     https://yoursite.com
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Notifications\Notificaciones;
use Illuminate\Support\Facades\Auth;
use App\Gasto;
use Validator;

/**
 * Usuario Controller
 * 
 * @category Controller
 * @package  App\Http\Controllers
 * @author   Carlos <carlosflres095@gmail.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
 * @link     https://yoursite.com
 */
class StorageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:usuarios');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gastos = Gasto::all();
        return view('xmllectura.uploadfile', compact('gastos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request The request object
     * 
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
       return view('xmllectura.form');
    }

    public function checkXml(Request $request)
    {
        if ($request->hasFile('file')) {

            if (isset($_FILES['file'])) {
                $file_name = $_FILES['file']['name'];
                $file_size = $_FILES['file']['size'];
                $file_tmp = $_FILES['file']['tmp_name'];
                $file_type = $_FILES['file']['type'];
                $fileNameCmps = explode(".", $file_name);
                $file_ext = strtolower(end($fileNameCmps));
                $extensions= array("xml");

                if (in_array($file_ext, $extensions)=== false) {
                    return redirect('/upload')
                    ->with('filealert', 'El Archivo No es XML');
                }
            }

            $file = $request->file('file');
            $nombre = $file->getClientOriginalName();
            \Storage::disk('local')->put($nombre,  \File::get($file));

            if (file_exists(public_path('storage/xmlfile.xml'))) {
                \Storage::delete('xmlfile.xml');
                \Storage::move($nombre, 'xmlfile.xml');

                $xml = simplexml_load_file("storage/xmlfile.xml"); 
                $ns = $xml->getNamespaces(true);

                if (isset($ns['cfdi'])) {
                    $xml->registerXPathNamespace('c', $ns['cfdi']);
                } else {
                    return redirect('/upload')
                    ->with('filealert', 'Archivo No Cumple Requisitos Version CFDI es Menor a 3.3');
                }
                if (isset($ns['tfd'])) {
                    $xml->registerXPathNamespace('t', $ns['tfd']);
                } else {
                    return redirect('/upload')
                    ->with('filealert', 'Archivo No Cumple Requisitos Version CFDI es Menor a 3.3');
                }        
                    return redirect('/generar_gastoxml')
                    ->with('success', 'Archivo Cargado Correctamente');
            
            } else {
                \Storage::move($nombre, 'xmlfile.xml');
                
                $xml = simplexml_load_file("storage/xmlfile.xml"); 
                $ns = $xml->getNamespaces(true);

                if (isset($ns['cfdi'])) {
                    $xml->registerXPathNamespace('c', $ns['cfdi']);
                } else {
                    return redirect('/upload')
                    ->with('filealert', 'Archivo No Cumple Requisitos Version CFDI es Menor a 3.3');
                }
                if (isset($ns['tfd'])) {
                    $xml->registerXPathNamespace('t', $ns['tfd']);
                } else {
                    return redirect('/upload')
                    ->with('filealert', 'Archivo No Cumple Requisitos Version CFDI es Menor a 3.3');
                }
                    return redirect('/generar_gastoxml')
                    ->with('success', 'Archivo Cargado Correctamente');
            }
        
        } else {
            return redirect('/upload')
            ->with('filealert', 'Archivo No Fue Cargado Correctamente');
        }      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request The request object
     * 
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $gastos = new Gasto;

        $v = \Validator::make(
            $request->all(), [
            'concepto' => 'required',
            'memo' => 'required',
            'tipopago' => 'required',
            'pais' => 'required',
            'comentario' => 'required',
            'fecha' => 'required|date',
            'tipocambio' => 'required',
            'moneda' => 'required',
            'monto' => 'required',
            'status' => 'required',
            'usuarioid' =>'required',
            'empresaid' => 'required',
            
            ]
        );

        $user = Auth::user(); 
        if ($v->fails()) {
            $user->notify(new Notificaciones('El formulario de gastos ha quedado incompleto, puedes terminar de llenarlo más adelante.'));
            return redirect()->back()->withInput()->withErrors($v->errors());
        }

        //$usuario->create($request->all());
        $gastos->usuario_id = $request->input('usuarioid');
        $gastos->empresa_id = $request->input('empresaid');
        $gastos->concepto = $request->input('concepto');
        $gastos->memo = $request->input('memo');
        $gastos->tipo_pago = $request->input('tipopago');
        $gastos->pais = $request->input('pais');
        $gastos->comentario = $request->input('comentario');
        $gastos->fecha = $request->input('fecha');
        $gastos->tipo_cambio = $request->input('tipocambio');
        $gastos->moneda = $request->input('moneda');
        $gastos->monto = $request->input('monto');
        $gastos->estatus = $request->input('status');
        $gastos->save();
        $gastos = Gasto::all();

        
        $user->email = "erick.95.10@hotmail.com";
        $user->notify(new Notificaciones('', 'mail'));
        return redirect('/upload')
                ->with('success', 'Gasto Generado');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param id $id The request object
     * 
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param id $id The request object
     * 
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
