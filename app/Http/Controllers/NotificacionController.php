<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notifications\Notificaciones;
use App\Usuario;
use Illuminate\Support\Facades\Auth;

class NotificacionController extends Controller
{

    public function __construct ()
    {
        $this->middleware('auth:usuarios');
    }

    public function index (Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required', 'lastname' => 'required', 'description' => 'required'
        ]);

        $user = Auth::user(); 
        if ($validator->fails()) {
            $user->notify(new Notificaciones('El formulario de gastos ha quedado incompleto, puedes terminar de llenarlo más adelante.')); 
        }
        else{
            /* Lógica para mandar por Email */
            $user->email = "erick.95.10@hotmail.com";
            $user->notify(new Notificaciones('', 'mail'));
        }
    }

    /**
     * Mostrar notificación 
     * 
     * @return view
     */
    public function show ($id = '')
    {
        $notifications = Auth::user()->notifications;

        return $this->notifications($id, $notifications);
    }

    private function notifications ($id, $notifications)
    {
        
        $notifications = empty($id) ? $notifications : 
                         $notifications->where('id', $id);
        return !$notifications->isEmpty() ? $this->markAsRead($id, $notifications) 
        : back();
    }

    private function markAsRead ($id, $notifications)
    {
        $unread = Auth::user()->unreadNotifications;

        if (empty($id)) {
            $read = $notifications->where('read_at', '!=', '');
        }
        else {
            $read = $notifications->where('id', $id)->where('read_at', '!=', '');
            $unread = $unread->where('id', $id);
        }


        if (!$unread->isEmpty()) {
            empty($id) ? $unread->markAsRead() : 
            $unread->where('id', $id)->markAsRead();
        }

        $user = Usuario::find(Auth::user()->id);
        $user->save();
        Auth::setUser($user);

        return view('nuevousuario.Notificaciones.notificaciones')
        ->with(['read' => $read, 'unread' => $unread]);
    }
}
