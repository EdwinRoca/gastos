<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use App\Usuario;
use App\Empresa;

class ActualizarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:usuarios')->except('logout');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = Usuario::all();
        $empresas = DB::table('empresas')->get();

        return view('usuarios.actualizar_lista', compact('usuarios', 'empresas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuarios = Usuario::find($id);
        $empresas = DB::table('empresas')->get();

        return view('usuarios.actualizar', compact ('usuarios', 'empresas'));

        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $usuario = Usuario::find($id);
        $data = $request->validate(
        [
            'nombre' => 'required|string',
            'email' => ['required', 'email', Rule::unique('usuarios')->ignore($usuario->id)],
            // 'email' => 'required|email|unique:usuarios',
            // 'banco' => 'required',
            // 'cuenta' => 'required|integer|digits_between:1,34',
            // 'clabe' => 'required|integer|digits:18',
            'departamento' => 'required|integer',
            'orden_servicio' => 'required|integer',
            'estatus' => 'required',
            'empresas_id' => 'required|integer',
            'categorias_id' => 'required|integer' 
        ]
        );
        
        $usuario->nombre = $request->get('nombre');
        $usuario->email = $request->get('email');
        $usuario->departamento= $request->get('departamento');
        $usuario->orden_servicio = $request->get('orden_servicio');
        $usuario->estatus = $request->get('estatus');
        $usuario->empresas_id = $request->get('empresas_id');
        $usuario->categorias_id = $request->get('categorias_id');

        if($request->get('password') == !Null){
            $usuario->password = bcrypt($request->get('password'));
        }
        
        $usuario->save();

        // $data = request()->validate(
        //     [
        //     'nombre'=> 'required|string',
        //     'email' => 'required|email|unique:usuarios',
        //     // 'banco' => 'required',
        //     // 'cuenta' => 'required|integer|digits_between:1,34',
        //     // 'clabe' => 'required|integer|digits:18',
        //     'departamento' => 'required|integer',
        //     'orden_servicio' => 'required|integer',
        //     'estatus' => 'required',
        //     'empresas_id' => 'required|integer',
        //     'categorias_id' => 'required|integer',
        //     'password' => 'required'
        //     ]
        // );

        // if ($data['password'] != null){
        //     $data['password'] = bcrypt($data['password']);
        // }else{
        //     unset($data['password']);
        // }

        // $usuario->update($data);

        return redirect('/usuarios/actualizar')
                ->with('success', 'Usuario actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
