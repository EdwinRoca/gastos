<?php
/**
 * PHP Version 7.2.10
 * 
 * @category Controller
 * @package  Database\Seeds
 * @author   Carlos <carlosflres095@gmail.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
 * @link     https://yoursite.com
 */
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Seeder
 * 
 * @category Controller
 * @package  Database\Seeds
 * @author   Carlos <carlosflres095@gmail.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
 * @link     https://yoursite.com
 */
class EmpresasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('empresas')->insert(
            [
                'nombre'=> 'Intekel',
                'direccion'=> 'Conocida',
                'gerente'=> 'Farid',
                'telefono'=> 12345,
                'email'=> 'gerencia@intekel.com',
                'estatus'=> 'Activo',
            ]
        );
    }
}
