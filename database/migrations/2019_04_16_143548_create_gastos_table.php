<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGastosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gastos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('usuario_id');
            $table->unsignedBigInteger('empresa_id');
            $table->string('archivo')->nullable();
            $table->string('concepto')->nullable();
            $table->string('memo')->nullable();
            $table->string('tipo_pago')->nullable();
            $table->string('pais')->nullable();
            $table->string('comentario')->nullable();
            $table->string('fecha')->nullable();
            $table->integer('tipo_cambio')->nullable();
            $table->string('moneda')->nullable();
            $table->integer('monto')->nullable();
            $table->string('estatus')->nullable();  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gastos');
    }
}
