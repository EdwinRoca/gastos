<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Route::get('/Iniciar_Sesion', 'usuariosController@showLoginForm');
// Route::post('/Iniciar_Sesion', 'usuariosController@login');
// Route::post('/Salir', 'usuariosController@logout')->name('salir');
// Route::get('/inicio', 'indexController@index')->name('inicio');

/*---------------- CREAR NUEVO USUARIO ------------- */
Route::resource('/usuarios/nuevo', 'UsuarioController');
Route::post('/usuarios/nuevo/crear', 'UsuarioController@store'); 





/*----------------------- ACTUALIZAR USUARIO-------------------*/
Route::resource('/usuarios/actualizar', 'ActualizarController');

/*-------------------------- ELIMINAR USUARIOS ---------------------------*/
Route::resource('/usuarios/eliminar', 'EliminarController');


Route::get('login', 'LoginUsuarioController@showLoginForm')->name('login');
Route::post('login', 'LoginUsuarioController@login');
Route::post('/Salir', 'LoginUsuarioController@logout');

/* ---------- Activar y desactiviar usuarios --------- */
Route::resource('/usuarios/activos', 'ActivarUsuarioController');

/* ---------- Lista de usuarios ---------- */
Route::resource('/usuarios/lista', 'ListaUsuariosController');

//---------Registro Usuario ------//
Route::resource('usuario', 'UsuarioController');

/*---------------- CREAR NUEVO USUARIO ------------- */
Route::resource('/usuarios/nuevo', 'UsuarioController');
Route::post('/usuarios/nuevo/crear', 'UsuarioController@store');
Route::post('usuario/crear', 'UsuarioController@store');

/*------------------Resetear Contraseña-------------------------------------------------------------------------*/
Route::group(
    ['prefix' => 'password-reset'], function () {
        Route::get('', 'UsuarioController@requestPasswordReset')
        ->name("RequestpasswordReset");
        Route::post('sendEmail', 'Auth\ForgotPasswordController@sendResetLinkEmail')
        ->name('send_email');
        Route::get('reset/{token}', 'Auth\ResetPasswordController@showResetForm')
        ->name('password_reset');
    }
);

/* Rutas de Notificaciones */
Route::view('form', 'nuevousuario.Notificaciones.form')->name('viewForm');
Route::post('form/enviar', 'NotificacionController@index')->name('form');
Route::get('notificaciones/{id?}', 'NotificacionController@show')
->name('notificaciones.show');

/* ----------------- Gastos ------------------ */
Route::resource('/gastos', 'GastosController');
Route::get('generar_gasto', 'GastosController@create');
Route::post('generar_gasto/crear_1', 'GastosController@store');
Route::post('generar_gasto/crear_2', 'GastosController@store_2');


/* ----------------- SOLO VISTAS ------------------ */
// Route::get('/recuperar', function(){
//     return view('recuperacion');
// });

Route::get('/inicio', function(){
    return view('index');
})->middleware('auth:usuarios')->name('inicio');

Route::get('/usuarios', function(){
    return view('usuarios.usuarios');
})->middleware('auth:usuarios');


// Route::get('/usuarios/nuevo', function(){
//     return view('usuarios.crear_usuario');
// });

Route::get('/generar_gastoxml', function(){
    return view('xmllectura.form');
});

Route::resource('upload', 'StorageController');
Route::post('upload/check', 'StorageController@checkXml');
Route::get('generar_gastoxml', 'StorageController@create');
Route::post('generar_gastoxml/crear', 'StorageController@store');

//Route::get('/usuarios/lista', function(){
//    return view('usuarios.lista');
//});
// Route::get('/usuarios/lista', function(){
//     return view('usuarios.lista');
// });

// Route::get('/usuarios/actualizar',function(){
//     return view('usuarios.actualizar_lista');
// });

// Route::get('/usuarios/actualizar/id', function(){
//     return view('usuarios.actualizar');
// });

// Route::get('/usuarios/permisos', function(){
//     return view('usuarios.permisos_lista');
// });

// Route::get('/usuarios/permisos/id', function(){
//     return view('usuarios.permisos');
// });

// Route::get('/usuarios/activos', function(){
//     return view('usuarios.usuarios_activos');
// });

